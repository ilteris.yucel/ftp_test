from ftp import connect_server, quit_server, list_server


class FTPService:
    def __init__(self):
        super(FTPService, self).__init__()
        self.connection = None
        self.status = "unconnected"
        self.server_list = []

    def connect_server(self, server, username, password):
        self.connection = connect_server(server, username, password)
        if self.connection is not None:
            self.status = "connected"
        else:
            self.status = "unconnected"

    def quit_server(self):
        if self.status != "connected":
            return
        quit_server(self.connection)
        self.status = "unconnected"

    def list_server(self):
        if self.status == "connected":
            self.server_list = list_server(self.connection)
        return self.server_list

    def is_connected(self):
        return self.status == "connected"
