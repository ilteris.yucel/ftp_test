from PyQt5.QtWidgets import QHBoxLayout, QLineEdit, QPushButton
from PyQt5.QtCore import Qt, pyqtSignal


class ConnectLayout(QHBoxLayout):
    server_connected = pyqtSignal(bool, name='connection_established')

    def __init__(self):
        super(ConnectLayout, self).__init__()
        self.ftp_service = None
        self.hostname_input = QLineEdit()
        self.hostname_input.setPlaceholderText("HOSTNAME >")

        self.username_input = QLineEdit()
        self.username_input.setPlaceholderText("USERNAME >")

        self.password_input = QLineEdit()
        self.password_input.setPlaceholderText("PASSWORD >")

        self.port_input = QLineEdit()
        self.port_input.setPlaceholderText("PORT >")

        self.connect_button = QPushButton("Connect")
        self.connect_button.clicked.connect(self.connect_ftp_server)
        self.detail_config_button = QPushButton('Detail Config')

        self.addWidget(self.hostname_input, 20)
        self.addWidget(self.username_input, 20)
        self.addWidget(self.password_input, 20)
        self.addWidget(self.port_input, 10)
        self.addWidget(self.connect_button, 10)
        self.addWidget(self.detail_config_button, 20)

    def connect_ftp_server(self):
        if self.ftp_service is None:
            print("FTP Service unavailable")
            return
        self.ftp_service.connect_server(
            self.hostname_input.text(),
            self.username_input.text(),
            self.password_input.text()
        )
        if self.ftp_service.is_connected():
            print("FTP connection established")
            self.server_connected.emit(self.ftp_service.is_connected())
        else:
            print("FTP connection cannot established")
