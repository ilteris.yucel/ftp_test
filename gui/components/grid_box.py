from PyQt5.QtWidgets import QGridLayout, QLabel, QTreeView
from PyQt5.QtCore import Qt, pyqtSlot


class GridBox(QGridLayout):
    def __init__(self):
        super(GridBox, self).__init__()
        self.ftp_service = None
        self.cel_0 = QLabel("CEL 1")
        self.cel_1 = QLabel("CEL 2")
        self.cel_2 = QLabel("CEL 3")
        self.cel_3 = QLabel("CEL 4")

        self.cel_0.setAlignment(Qt.AlignCenter)
        self.cel_1.setAlignment(Qt.AlignCenter)
        self.cel_2.setAlignment(Qt.AlignCenter)
        self.cel_3.setAlignment(Qt.AlignCenter)

        self.addWidget(self.cel_0, 0, 0)
        self.addWidget(self.cel_1, 0, 1)
        self.addWidget(self.cel_2, 1, 0)
        self.addWidget(self.cel_3, 1, 1)

    @pyqtSlot(bool)
    def show_ftp_server(self, status):
        print("Show ftp server", status)
        print(self.ftp_service.list_server())
