import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QMainWindow, QMenu, QBoxLayout
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5 import QtCore
from gui.components import grid_box as gb
from gui.components import connect_layout as cl
from services import ftp_service as fs

MAX_WIDTH = 960
MAX_HEIGHT = 640


class MainWindow(QMainWindow):
    server_connected = pyqtSignal(bool, name='connection_established')

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle("ShittyZilla")
        self.setGeometry(0, 0, MAX_WIDTH, MAX_HEIGHT)
        self.setMaximumWidth(MAX_WIDTH)
        self.setMaximumHeight(MAX_HEIGHT)
        self.setMinimumWidth(MAX_WIDTH)
        self.setMinimumHeight(MAX_HEIGHT)
        self._create_menu_bar()
        self.main_widget = QWidget()
        self.main_layout = QBoxLayout(QBoxLayout.TopToBottom)
        self.connect_layout = cl.ConnectLayout()
        self.grid_layout = gb.GridBox()
        self.main_layout.addLayout(self.connect_layout)
        self.main_layout.addLayout(self.grid_layout)
        self.main_widget.setLayout(self.main_layout)
        self.setCentralWidget(self.main_widget)
        self.main_widget.move(0, 0)
        self.main_widget.resize(MAX_WIDTH, MAX_HEIGHT)
        self.ftp_service = fs.FTPService()
        self.connect_layout.ftp_service = self.ftp_service
        self.grid_layout.ftp_service = self.ftp_service
        self.connect_layout.server_connected.connect(self.connection_established_slot)
        self.server_connected.connect(self.grid_layout.show_ftp_server)
        self.show()

    def _create_menu_bar(self):
        menu_bar = self.menuBar()
        settings_menu = QMenu("&Settings", self)
        menu_bar.addMenu(settings_menu)

        help_menu = QMenu("&Help", self)
        menu_bar.addMenu(help_menu)

        about_menu = QMenu("&About", self)
        menu_bar.addMenu(about_menu)

    def centered_label(self, text):
        cle = QLabel(text, self)
        cle.move(0, 0)
        cle.resize(self.frameGeometry().width(), self.frameGeometry().height())
        cle.setAlignment(Qt.AlignCenter)
        cle.show()

    def change_position(self, x, y):
        self.setGeometry(x, y, self.frameGeometry().width(), self.frameGeometry().height())

    @QtCore.pyqtSlot(bool)
    def connection_established_slot(self, status):
        print("Connection Established ", status)
        self.server_connected.emit(status)


def main():
    app = QApplication(sys.argv)
    screen = app.primaryScreen()
    screen_size = screen.size()
    main_widget = MainWindow()
    main_widget.change_position(screen_size.width() / 2 - main_widget.frameGeometry().width() / 2,
                                screen_size.height() / 2 - main_widget.frameGeometry().height() / 2)
    # main_widget.centered_label("SHITTYZILLA IS COMING")
    sys.exit(app.exec())


if __name__ == "__main__":
    main()
