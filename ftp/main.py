import ftplib


def connect_server(server, username, password):
    try:
        ftp_server = ftplib.FTP_TLS(server)
        ftp_server.login(username, password)
        ftp_server.prot_p()
    except ftplib.all_errors as e:
        print("Error on connection")
        print(e)
        return None
    return ftp_server


def quit_server(ftp_server):
    ftp_server.quit()
    return None


def list_server(ftp_server):
    server_list = []
    ftp_server.dir(server_list.append)
    return server_list


def main():
    hostname = input("Please enter hostname >")
    username = input("Please enter username >")
    password = input("Please enter password >")

    ftp_server = connect_server(hostname, username, password)
    if ftp_server is None:
        print("Connection cannot established")
        return
    status = True
    while status:
        op = input("Type l to list server, q to quit >")
        if op == "q":
            quit_server(ftp_server)
            status = False
        elif op == "l":
            for f in list_server(ftp_server):
                print(f)
        else:
            print("Unknown command")


if __name__ == "__main__":
    main()
